# Text encryption-decription
CLI app created to enable text encryption-decryption with password. Using AES.

# Requirements
* Node.js (=> 14)
* Yarn

# Pre-use / Installation

```
# Install required modules

yarn
```

# Usage
### Encrypt text with password
`yarn encrypt 'password' 'input text'`


### Decrypt text with password
`yarn decrypt 'password' 'input text'`