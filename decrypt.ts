import { Command } from 'commander';
import * as CryptoJS from 'crypto-js';

(async () => {
    try {
        const program = new Command();

        program
            .name('decrypt')
            .version('1.0.0')
            .argument('<password>', 'Password for text decryption')
            .argument('<input>', 'Input text for decryption')
            .action((password, input) => {
                if (password === '' || input === '') {
                    throw new Error('Required input (password, input text) is missing.');
                }
                // Decrypt text
                const decryptedText = CryptoJS.AES.decrypt(input, password).toString(CryptoJS.enc.Utf8);

                console.log(`\nDecrypted text:\n${decryptedText}\n\n`);
            })
            .parse(process.argv);
    } catch (err: any) {
        console.warn(`Encryption app error: ${err.message}`);
    }
})();