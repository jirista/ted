import { Command } from 'commander';
import * as CryptoJS from 'crypto-js';

(async () => {
    try {
        const program = new Command();

        program
            .name('encrypt')
            .version('1.0.0')
            .argument('<password>', 'Password for text encryption')
            .argument('<input>', 'Input text to encrypt')
            .action((password, input) => {
                if (password === '' || input === '') {
                    throw new Error('Required input (password, input text) is missing.');
                }
                // Encrypt text
                const cipherText = CryptoJS.AES.encrypt(input, password).toString();
                console.log(`-- Password: ${password}`);
                console.log(`-- Input text: ${input}\n`);
                console.log(`Encrypted text:\n${cipherText}`);
                console.log("\n");
                // Validate encrypted text
                const decryptedText = CryptoJS.AES.decrypt(cipherText, password).toString(CryptoJS.enc.Utf8);
                if (input === decryptedText) {
                    console.log('[OK] Input and encrypted text equation successfully validated!');
                } else {
                    throw new Error('[KO] Input and encrypted text is not equal - internal error');
                }
            })
            .parse(process.argv);
    } catch (err: any) {
        console.warn(`Encryption app error: ${err.message}`);
    }
})();